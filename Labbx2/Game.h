//
//  Game.h
//  Labbx2
//
//  Created by Anton Nilsson on 2016-02-06.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Game : NSObject

@property (nonatomic) NSUInteger currentQuestion;
@property (nonatomic) NSUInteger currentScore;


-(void) shuffle;
-(NSDictionary *) question;
-(BOOL) guess: (NSString *)guess;
-(NSUInteger) score;
-(BOOL) gameCompleted;
    
@end
