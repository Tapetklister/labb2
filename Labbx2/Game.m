//
//  Game.m
//  Labbx2
//
//  Created by Anton Nilsson on 2016-02-06.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "Game.h"

@interface Game()
@property (nonatomic) NSMutableArray *questionList;
@property (nonatomic) NSDictionary *question1;
@property (nonatomic) NSDictionary *question2;
@property (nonatomic) NSDictionary *question3;
@property (nonatomic) NSDictionary *question4;
@property (nonatomic) NSDictionary *question5;
@property (nonatomic) NSDictionary *question6;
@property (nonatomic) NSDictionary *question7;
@property (nonatomic) NSDictionary *question8;
@property (nonatomic) NSDictionary *question9;
@property (nonatomic) NSDictionary *question10;
@end;

@implementation Game

-(instancetype)init {
    self = [super init];
    if(self) {
        self.question1 = @{@"question": @"What is the name of Super Mario's green-clad brother?",
                           @"correct": @"Luigi",
                           @"incorrect": @[@"Wario", @"Leonardo", @"Giovanni"], @"hint": @"Starts with an L"};
        self.question2 = @{@"question": @"Who is the protagonist in Halo: Combat Evolved?",
                           @"correct": @"Master Chief",
                           @"incorrect": @[@"Ezio Auditore", @"Nathan Drake", @"Commander Shepard"], @"hint": @"His title starts with 'Master' and ends with 'Chief'"};
        self.question3 = @{@"question": @"When was the original Pac-Man released?",
                           @"correct": @"1980",
                           @"incorrect": @[@"1970", @"1990", @"2000"], @"hint": @"One year before Super Mario Bros"};
        self.question4 = @{@"question": @"What is the name of Wario's mischiveous partner?",
                           @"correct": @"Waluigi",
                           @"incorrect": @[@"Birdo", @"Yoshi", @"Boo"], @"hint": @"His name means 'Bad Luigi' in Japanese"};
        self.question5 = @{@"question": @"Which of the following games was not fundraised?",
                           @"correct": @"P.T",
                           @"incorrect": @[@"Yooka-Laylee", @"Shovel Knight", @"Bloodstained"], @"hint": @"It's a pretty short title"};
        self.question6 = @{@"question": @"Which game has the highest score on Metacritic?",
                           @"correct": @"The Legend of Zelda: Ocarina of Time",
                           @"incorrect": @[@"Tony Hawk's Pro Skater 2", @"Super Mario Bros 3", @"GTA IV"], @"hint": @"It was originally released on Nintendo 64"};
        self.question7 = @{@"question": @"When was the Wii U released?",
                           @"correct": @"2012",
                           @"incorrect": @[@"2013", @"2011", @"2014"], @"hint": @"The world was supposed to end this year"};
        self.question8 = @{@"question": @"Which of the following is not a Bioware game?",
                           @"correct": @"The Witcher",
                           @"incorrect": @[@"Dragon Age II", @"Mass Effect", @"Star Wars: The Old Republic"], @"hint": @"The title does not contain an 'a'"};
        self.question9 = @{@"question": @"Which of the following characters have never been a playable character in the Super Smash Bros series?",
                           @"correct": @"Midna",
                           @"incorrect": @[@"Ryu", @"Bayonetta", @"Solid Snake"], @"hint": @"She is the shortest of the four"};
        self.question10 = @{@"question": @"Who is the creator of The Legend of Zelda?",
                            @"correct": @"Shigeru Myamoto",
                            @"incorrect": @[@"Saturo Iwata", @"Hideo Kojima", @"Toru Iwatani"], @"hint": @"It is the same person that created Super Mario and Donkey Kong"};
        
        self.questionList =  [NSMutableArray arrayWithObjects: self.question1, self.question2, self.question3, self.question4, self.question5, self.question6, self.question7, self.question8, self.question9, self.question10, nil];
        
        self.currentQuestion = 0;
        self.currentScore = 0;
        
    } return self;
}

-(void) shuffle {
    for(NSUInteger i=self.questionList.count; i>1; i--) {
        [self.questionList exchangeObjectAtIndex:i-1 withObjectAtIndex:arc4random_uniform(i)];
    }
}

-(NSDictionary *) question {
    return [self.questionList objectAtIndex: (self.currentQuestion)];
}

-(BOOL) guess: (NSString *)guess{
    if(guess == [self.questionList objectAtIndex: self.currentQuestion ][@"correct"]) {
        self.currentQuestion++;
        self.currentScore++;
        return YES;
    } else {
        self.currentQuestion++;
        return NO;
    }
}

-(NSUInteger) score {
    return self.currentScore;
}

-(BOOL) gameCompleted {
    if(self.currentQuestion<10) {
        return NO;
    } else {
        return YES;
    }
}

@end
