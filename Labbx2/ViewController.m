//
//  ViewController.m
//  Labbx2
//
//  Created by Anton Nilsson on 2016-02-06.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "Game.h"
#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) Game *game;
@property (weak, nonatomic) IBOutlet UILabel *questionView;
@property (weak, nonatomic) IBOutlet UIButton *button1Layout;
@property (weak, nonatomic) IBOutlet UIButton *button2Layout;
@property (weak, nonatomic) IBOutlet UIButton *button3Layout;
@property (weak, nonatomic) IBOutlet UIButton *button4Layout;
@property (weak, nonatomic) IBOutlet UIButton *iLikeBigButtonsAndICanNotLie;
@property (nonatomic) BOOL guessResult;
@property (nonatomic) NSMutableArray *array;
@property (nonatomic) NSDictionary *question;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.iLikeBigButtonsAndICanNotLie.hidden = YES;
    self.game = [[Game alloc]init];
    [self.game shuffle];
    self.question = [[NSDictionary alloc]init];
    self.array = [[NSMutableArray alloc]init];
    [self setButtonValues];
}

-(void) shuffle {
    for(NSUInteger i=self.array.count; i>1; i--) {
        [self.array exchangeObjectAtIndex:i-1 withObjectAtIndex:arc4random_uniform(i)];
    }
}
- (IBAction)buttonPress1:(UIButton *)sender {
    self.guessResult = [self.game guess:self.array[0]];
    [self result : (BOOL)self.guessResult];
}
- (IBAction)buttonPress2:(UIButton *)sender {
    self.guessResult = [self.game guess:self.array[1]];
    [self result : (BOOL)self.guessResult];
}
- (IBAction)buttonPress3:(UIButton *)sender {
    self.guessResult = [self.game guess:self.array[2]];
    [self result : (BOOL)self.guessResult];
}
- (IBAction)buttonPress4:(UIButton *)sender {
    self.guessResult = [self.game guess:self.array[3]];
    [self result : (BOOL)self.guessResult];
}
- (IBAction)hint:(UIButton *)sender {
    UIAlertView *hintAlert = [[UIAlertView alloc]initWithTitle:@"Hint" message:[self.question objectForKey:@"hint"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [hintAlert show];
}

-(void) result : (BOOL ) guessBool{
    
    [self hideOrShow];
    
    if (guessBool) {
        [self.iLikeBigButtonsAndICanNotLie setTitle:@"You guessed correctly!" forState:UIControlStateNormal];
    } else {
        [self.iLikeBigButtonsAndICanNotLie setTitle:@"You guessed wrong!" forState:UIControlStateNormal];
    }
}
- (IBAction)bigButtonPress:(UIButton *)sender {
    if(!self.game.gameCompleted) {
        [self hideOrShow];
        [self setButtonValues];
    } else {
        [self.iLikeBigButtonsAndICanNotLie setTitle: [NSString stringWithFormat:@"You got %d points!", self.game.currentScore] forState:UIControlStateNormal];
    }
}

-(void) setButtonValues {
    
    
    self.question = [self.game question];
        
    self.array[0] = [self.question objectForKey:@"correct"];
    self.array[1] = [self.question objectForKey:@"incorrect"][0];
    self.array[2] = [self.question objectForKey:@"incorrect"][1];
    self.array[3] = [self.question objectForKey:@"incorrect"][2];
        
    self.questionView.text = [self.question objectForKey:@"question"];
        
    [self shuffle];
        
    [self.button1Layout setTitle: self.array[0] forState:UIControlStateNormal];
    [self.button2Layout setTitle: self.array[1] forState:UIControlStateNormal];
    [self.button3Layout setTitle: self.array[2] forState:UIControlStateNormal];
    [self.button4Layout setTitle: self.array[3] forState:UIControlStateNormal];
}

-(void) hideOrShow {
    self.button1Layout.hidden = !self.button1Layout.hidden;
    self.button2Layout.hidden = !self.button2Layout.hidden;
    self.button3Layout.hidden = !self.button3Layout.hidden;
    self.button4Layout.hidden = !self.button4Layout.hidden;
    self.iLikeBigButtonsAndICanNotLie.hidden = !self.iLikeBigButtonsAndICanNotLie.hidden;
}

@end
