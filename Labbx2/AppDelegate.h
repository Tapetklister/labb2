//
//  AppDelegate.h
//  Labbx2
//
//  Created by Anton Nilsson on 2016-02-06.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

